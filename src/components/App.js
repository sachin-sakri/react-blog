import React, { Component } from 'react';
import AddPost from '../containers/AddPost';
import PostList from '../containers/PostList';

class App extends Component {
	render() {
		return (
			<div>
				<header>
					<nav className='navbar navbar-dark bg-white shadow-sm p-3'>
						<h2 className='text-dark'>React - Blog CRUD</h2>
						<ul className='nav'>
							<li className='nav-item'>
								<a
									target='blank'
									className='nav-link color-linkedin'
									href='https://www.linkedin.com/in/sachin-sakri-4ba82910a/'
								>
									<i className='fab fa-linkedin-in'></i>
								</a>
							</li>
							<li className='nav-item'>
								<a
									target='blank'
									className='nav-link color-github'
									href='https://github.com/sachin-sakri-au5'
								>
									<i className='fab fa-github'></i>
								</a>
							</li>
						</ul>
					</nav>
				</header>
				<div className='container-fluid'>
					<div className='row mt-4'>
						<div className='col-md-4 '>
							<AddPost />
						</div>
						<div className='col-md-8'>
							<div className='row'>
								<div className='col-md-12'>
									<PostList />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default App;
